import { NgModule } from '@angular/core';
import { NgxMaskModule } from 'ngx-mask';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { PAGES } from './pages';
import { APP_COMPONENTS } from './components';
import { AppComponent } from './app.component';
import { SHARED_SERVICES } from './shared/services';
import { AppRoutingModule } from './app-routing.module';
import { SHARED_COMPONENTS } from './shared/components';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    PAGES,
    AppComponent,
    APP_COMPONENTS,
    SHARED_COMPONENTS,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [
    SHARED_SERVICES,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
