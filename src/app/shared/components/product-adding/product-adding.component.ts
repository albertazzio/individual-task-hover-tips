import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Product } from '../../models/api';
import { ModalProductConfig } from '../../models/modal-product-config';

@Component({
  selector: 'app-product-adding',
  templateUrl: './product-adding.component.html',
  styleUrls: ['./product-adding.component.scss'],
})
export class ProductAddingComponent {
  @Input() public editingProduct: Product;
  @Input() public products: Product[] = [];
  @Input() public categories: string[] = [];
  @Input() public isProductModalShow: boolean = false;
  @Input() public changeProductData: ModalProductConfig;

  @Output() public closeAddingForm: EventEmitter<void> = new EventEmitter<void>();
  @Output() public addNewProduct: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() public showDataByCategory: EventEmitter<string> = new EventEmitter<string>();
}

