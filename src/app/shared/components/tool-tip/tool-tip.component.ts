import { Component } from '@angular/core';

import { HoverTipsService } from '../../services/hover-tips.service/hover-tips.service';
import { NotificationModalService } from 'src/app/shared/services/notification-modal.service/notification-modal.service';

@Component({
  selector: 'app-tool-tip',
  templateUrl: './tool-tip.component.html',
  styleUrls: ['./tool-tip.component.scss'],
})

export class ToolTipComponent {
  constructor (
    public hoverTipsService: HoverTipsService,
    public notificationModalService: NotificationModalService,
  ) {}
}
