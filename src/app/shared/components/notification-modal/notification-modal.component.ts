import { Component } from '@angular/core';

import { NotificationModalService } from 'src/app/shared/services/notification-modal.service/notification-modal.service';

@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.scss']
})
export class NotificationModalComponent {
  constructor(
    public notificationModalService: NotificationModalService,
  ) {}
}
