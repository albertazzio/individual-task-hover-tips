import { Component } from '@angular/core';

import { NewFormEvent } from '../../models/validation-form';

@Component({
  selector: 'validation-form',
  templateUrl: './validation-form.component.html',
  styleUrls: ['./validation-form.component.scss']
})
export class ValidationFormComponent {
  public newData: NewFormEvent = new NewFormEvent();

  public addNewForm(value: NewFormEvent): void {
    console.log(value);
  }
}

