import { Component } from '@angular/core';

import { ProductsService } from '../../services/products.service/products.service';

@Component({
  selector: 'confirm-remove-product-modal',
  templateUrl: './confirm-remove-product-modal.component.html',
  styleUrls: ['./confirm-remove-product-modal.component.scss']
})
export class ConfirmRemoveProductModalComponent {
  constructor(
    public productsService: ProductsService,
  ) {}
}
