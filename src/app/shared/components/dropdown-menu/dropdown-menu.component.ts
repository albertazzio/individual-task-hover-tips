import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Product } from '../../models/api';

@Component({
  selector: 'dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.scss'],
})
export class DropdownMenuComponent {
  @Input() public editingProduct: Product;
  @Input() public categories: string[] = [];

  @Output() public showDataByCategory: EventEmitter<string> = new EventEmitter<string>();

  public selectedCategory: string;
}
