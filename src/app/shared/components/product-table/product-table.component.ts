import { Component, Output, EventEmitter, Input } from '@angular/core';

import { Product } from '../../models/api';
import { ADDING_PRODUCT_MODAL_DATA } from '../../constants/adding-product-config';
import { EDITING_PRODUCT_MODAL_DATA } from '../../constants/editing-product-config';
import { ModalProductChanging, ModalProductConfig } from '../../models/modal-product-config';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent {
  @Input() public editingProduct: Product;
  @Input() public products: Product[] = [];
  @Input() public categories: string[] = [];
  @Input() public isEditProduct: boolean = false;
  @Input() public errorMessage: string | undefined;
  @Input() public isProductModalShow: boolean = false;
  @Input() public changeProductData: ModalProductConfig;

  @Output() public filterByGender: EventEmitter<void> = new EventEmitter<void>();
  @Output() public closeAddingForm: EventEmitter<void> = new EventEmitter<void>();
  @Output() public addNewProduct: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() public resetAllDataProducts: EventEmitter<void> = new EventEmitter<void>();
  @Output() public showAllTableData: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() public showDataByCategory: EventEmitter<string> = new EventEmitter<string>();
  @Output() public removeProductData: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() public openProductModalEdit: EventEmitter<ModalProductChanging> = new EventEmitter<ModalProductChanging>();

  public readonly ADDING_PRODUCT_MODAL_DATA: ModalProductConfig = ADDING_PRODUCT_MODAL_DATA;
  public readonly EDITING_PRODUCT_MODAL_DATA: ModalProductConfig = EDITING_PRODUCT_MODAL_DATA;
}
