import { ToolTipComponent } from './tool-tip/tool-tip.component';
import { SpinnerComponent } from './spinner-component/spinner.component';
import { DropdownMenuComponent } from './dropdown-menu/dropdown-menu.component';
import { ProductTableComponent } from './product-table/product-table.component';
import { ProductAddingComponent } from './product-adding/product-adding.component';
import { ValidationFormComponent } from './validation-form/validation-form.component';
import { NotificationModalComponent } from './notification-modal/notification-modal.component';
import { ConfirmRemoveProductModalComponent } from './confirm-remove-product-modal/confirm-remove-product-modal.component';

export const SHARED_COMPONENTS = [
  ToolTipComponent,
  SpinnerComponent,
  DropdownMenuComponent,
  ProductTableComponent,
  ProductAddingComponent,
  ValidationFormComponent,
  NotificationModalComponent,
  ConfirmRemoveProductModalComponent,
]
