export class ModalConfig {
  title: string;
  bodyText: string;
  isCancel: boolean;
  isOkBtn: boolean;
  data: any;
}

export class ModalResponse {
  isOk: boolean;
  data: any;
}
