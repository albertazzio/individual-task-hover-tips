export class TooltipTextContent {
  name: string;
  isVisible: boolean;
  id: number;
}
