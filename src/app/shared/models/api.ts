import { getUniqId } from '../helpers/helpers';

export class ApiTableDataApi {
  count: number;
  entries: EntriesApi[];
}

export class EntriesApi {
  API: string;
  Auth: string;
  Category: string;
  Cors: string;
  Description: string;
  HTTPS: boolean;
  Link: string;
}

export class ProductData {
  count: number;
  entries: Product[];

  public static getDataFromApi(data: ApiTableDataApi): ProductData {
    return {
      count: data.count,
      entries: data.entries.map((val,index) => Product.getDataFromApi(val, data.entries.length, index)),
    }
  }
}

export class Product {
  author: string;
  api: string;
  category: string;
  cors: string;
  description: string;
  https: boolean;
  link: string;
  id: number;
  gender: string;

  public static getDataFromApi(data: EntriesApi, dataLength: number, itemIndex: number): Product {
    return {
      author: data.API,
      cors: data.Cors,
      link: data.Link,
      api: data.Auth,
      https: data.HTTPS,
      category: data.Category,
      description: data.Description,
      id: getUniqId(),
      gender: this.makeGender(dataLength, itemIndex),
    }
  }

  public static makeGender(dataLength: number, itemIndex: number): string {
    if (itemIndex < dataLength/2) {

      return 'men'
    } else {

      return 'women'
    }
  }
}

export class TestDataAPI {
  data: DataAPI[];
  isOk: boolean;
}

export class TestData {
  name: Data[];
  status: boolean;

  public static getTestDataFromAPI(data: TestDataAPI): TestData {
    return {
      name: data.data.map((val) => Data.getTestDataFromAPI(val)),
      status: data.isOk,
    }
  }
}

export class DataAPI {
  name: string;
  id: number;
  createdDateTime: string;
  updatedMemo: string;
  updatedDateTime: string;
}

export class Data {
  name: string;
  id: number;
  createdDateTime: string;
  updatedMemo: string;
  updatedDateTime: string;

  public static getTestDataFromAPI(data: DataAPI): Data {
    return {
      name: data.name,
      id: data.id,
      createdDateTime: data.createdDateTime,
      updatedMemo: data.updatedMemo,
      updatedDateTime: data.updatedDateTime,
    }
  }
}































































