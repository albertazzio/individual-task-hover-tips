export class NewFormEvent {
  firstName: string;
  lastName: string;
  emailAddress: string;
  telNumber: number;
  password: string;
}

