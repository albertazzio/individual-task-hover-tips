export class ModalTexts {
  title: string;
  bodyText: string;
  isCancel: boolean;
  isOkBtn: boolean;
  data: any;
}
