import { Product } from './api';

export class ModalProductConfig {
  title: string;
  isCancel: boolean;
  isSaveBtn: boolean;
  isAddBtn: boolean;
  data: any;
}

export class ModalProductChanging {
  config: ModalProductConfig;
  data: Product | null;
}
