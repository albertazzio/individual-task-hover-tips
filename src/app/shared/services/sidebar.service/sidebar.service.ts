import { Injectable } from '@angular/core';

import { LocalStorageService } from '../local-storage.service/local-storage.service';
import { POINT_FOR_COLLAPSE_SIDEBAR, POINT_FOR_HIDE_SIDEBAR } from '../../constants/inner-width';

@Injectable()
export class SidebarService {
  public isSidebarHide: boolean;
  public isCollapseSidebar: boolean;
  public isUserChoiceCollapse: boolean = false;

  public readonly LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR: string = 'isCollapseSidebar';

  constructor(
    public localStorageService: LocalStorageService,
  ) {}

  public getStatusCollapseSidebarFromLocalStorage(): void {
    this.isCollapseSidebar = this.localStorageService.getItem(this.LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR);
  }

  public collapseSidebarByResize(): void {
    this.isSidebarHide = SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR);
    this.isCollapseSidebar = this.localStorageService.getItem(this.LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR);

    if (this.localStorageService.getItem(this.LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR) === null) {
      this.setSidebarDefaultStatus();
    }

    if (this.isUserChoiceCollapse) return;
  }

  public changeSidebarStatus(): void {
    this.isUserChoiceCollapse = SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR);
    SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR) ? this.isSidebarHide = !this.isSidebarHide : this.isSidebarHide = false;
    SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR) ? this.isCollapseSidebar = false : this.isCollapseSidebar = !this.isCollapseSidebar;

    if (window.innerWidth > POINT_FOR_HIDE_SIDEBAR) {
      this.localStorageService.setItem(this.LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR, this.isCollapseSidebar)
    }
  }

  public setSidebarStatus(): void {
    this.getStatusCollapseSidebarFromLocalStorage();
    if (this.localStorageService.getItem(this.LOCAL_STORAGE_KEY_FOR_COLLAPSE_SIDEBAR) === null) {
      this.setSidebarDefaultStatus();
    }
    this.hideSidebarOnMobile();
  }

  public setSidebarDefaultStatus(): void {
    SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR) ? this.isSidebarHide = true : this.isSidebarHide = false;
    !SidebarService.getSidebarInnerWidth(POINT_FOR_COLLAPSE_SIDEBAR) ? this.isCollapseSidebar = false : this.isCollapseSidebar = true;
  }

  public hideSidebarOnMobile(): void {
    this.isSidebarHide = SidebarService.getSidebarInnerWidth(POINT_FOR_HIDE_SIDEBAR);
  }

  private static getSidebarInnerWidth(value: number): boolean {
    return window.innerWidth < value;
  }
}
