import { Injectable } from '@angular/core';

import { catchError, map, Observable, throwError } from 'rxjs';

import { UrlService } from '../url.service/url.service';
import { HttpService } from '../http.service/http.service';
import { ProductData, ApiTableDataApi, Product, TestData, TestDataAPI } from '../../models/api';
import { HttpErrorResponse } from '@angular/common/http';
import { createLogErrorHandler } from '@angular/compiler-cli/ngcc/src/execution/tasks/completion';

@Injectable()
export class ProductsService {
  public products: Product[] = [];
  public isTableData: boolean = false;
  public isShowConfirmRemoveModal: boolean = false;

  constructor(
    public httpService: HttpService,
    public urlService: UrlService,
  ) {}

  public ngOnInit(): void {
    this.IsTableData();
  }

  public IsTableData(): void {
    this.isTableData = this.products.length === 0;
  }

  public closeConfirmRemoveModal(): void {
    this.isShowConfirmRemoveModal = false;
  }

  public getProducts$(category: string): Observable<ProductData> {
    return this.httpService.getProduct(this.urlService.getProductUrl(category))
      .pipe(
        map((data: ApiTableDataApi): ProductData => ProductData.getDataFromApi(data)),
      catchError(this.handleError),
  )
  }

  public handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      console.error('An error  occurred', error.error);
    } else {
      console.error(`Backend returned code ${error.status}, body was:`, error.error);
    }
    return throwError(() => new Error('Something bad happened; please try again later.'));
  }

  public getTestData$(): Observable<TestData> {
    return this.httpService.getProduct(this.urlService.getProductUrl())
      .pipe(map((data: TestDataAPI): TestData => TestData.getTestDataFromAPI(data)))
  }
}
