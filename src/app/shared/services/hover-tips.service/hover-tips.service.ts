import { Injectable } from '@angular/core';

import { ModalConfig } from '../../models/modal-config';
import { TEXTS } from '../../constants/tooltip-text-content';
import { MODAL_TEXTS } from '../../constants/notification-modal';
import { TooltipTextContent } from '../../models/tooltip-text-content';

@Injectable()
export class HoverTipsService {
  public counter: number = 0;
  public modal: boolean = false;

  public readonly MODAL_TEXTS: ModalConfig = MODAL_TEXTS;
  public readonly ARR_TEXTS: TooltipTextContent[] = TEXTS;

  public isTextVisible(): void {
    this.ARR_TEXTS.forEach(item => {
      item.isVisible = item.id === this.counter;
    })
  }

  public isToggleTextNext(): void {
    this.counter++;
    this.isTextVisible();

    if (this.counter >= this.ARR_TEXTS.length) {
      this.counter = 0;
      this.isTextVisible();
    }
  }

  public isToggleTextPrev(): void {
    this.counter--;
    this.isTextVisible();

    if (this.counter < 0) {
      this.counter = this.ARR_TEXTS.length - 1;
      this.isTextVisible();
    }
  }
}
