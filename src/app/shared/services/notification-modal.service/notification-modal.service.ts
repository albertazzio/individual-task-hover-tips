import { Injectable, EventEmitter } from '@angular/core';

import { ModalConfig, ModalResponse } from '../../models/modal-config';

@Injectable()
export class NotificationModalService {
  public removeProduct: EventEmitter<ModalResponse> = new EventEmitter<ModalResponse>();
  public isShowModal: boolean = false;
  public confirmModalData: ModalConfig = new ModalConfig();

  public closeModal(data: ModalResponse): void {
    this.removeProduct.emit(data);
    this.confirmModalData = new ModalConfig();
    this.isShowModal = false;
  }

  public openModal(value: ModalConfig): void {
    this.confirmModalData = value;
    this.isShowModal = true;
  };
}
