import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {
  public isSpinnerActive: boolean = false;
}
