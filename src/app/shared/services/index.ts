import { UrlService } from './url.service/url.service';
import { HttpService } from './http.service/http.service';
import { SpinnerService } from './spinner.service/spinner.service';
import { SidebarService } from './sidebar.service/sidebar.service';
import { ProductsService } from './products.service/products.service';
import { HoverTipsService } from './hover-tips.service/hover-tips.service';
import { LocalStorageService } from './local-storage.service/local-storage.service';
import { NotificationModalService } from './notification-modal.service/notification-modal.service';

export const SHARED_SERVICES = [
  UrlService,
  HttpService,
  SpinnerService,
  SidebarService,
  ProductsService,
  HoverTipsService,
  LocalStorageService,
  NotificationModalService,
]
