import { SidebarTitle } from '../models/sidebar-title';

export const SIDEBAR_TITLES: SidebarTitle[] = [
  {
    text: 'New Request',
    svg: './assets/images/requests.svg',
    path: 'new-request',
  },
  {
    text: 'Patients',
    svg: './assets/images/patients.svg',
    path: 'patients',
  },
  {
    text: 'User Management',
    svg: './assets/images/users.svg',
    path: 'user-management',
  },
  {
    text: 'Users',
    svg: './assets/images/users.svg',
    path: 'users',
  },
  {
    text: 'My Profile',
    svg: './assets/images/profile.svg',
    path: 'my-profile',
  },
  {
    text: 'Accounts',
    svg: './assets/images/profile.svg',
    path: 'accounts',
  },
  {
    text: 'Suppliers',
    svg: './assets/images/suppliers.svg',
    path: 'suppliers',
  },
  {
    text: 'Products',
    svg: './assets/images/products.svg',
    path: 'products',
  },
  {
    text: 'Requests',
    svg: './assets/images/requests.svg',
    path: 'requests',
  },
  {
    text: 'Invoice Summary',
    svg: './assets/images/patients.svg',
    path: 'invoice-summary',
  },
  {
    text: 'Price Sheets',
    svg: './assets/images/price.svg',
    path: 'price-sheets',
  },
  {
    text: 'Price Sheet Search',
    svg: './assets/images/price.svg',
    path: 'price-sheet-search',
  },
]
