import { TooltipTextContent } from '../models/tooltip-text-content';

export const TEXTS: TooltipTextContent[] = [
  {
    name: 'Lorem Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1500s.',
    isVisible: true,
    id: 0
  },
  {
    name: 'Albert Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1510s.',
    isVisible: false,
    id: 1
  },
  {
    name: 'Marina Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1520s.',
    isVisible: false,
    id: 2
  },
  {
    name: 'Egor Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1530s.',
    isVisible: false,
    id: 3
  },
  {
    name: 'Slava Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1540s.',
    isVisible: false,
    id: 4
  },
  {
    name: 'Sasha Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1550s.',
    isVisible: false,
    id: 5
  },
  {
    name: 'Delen Ipsum is simply dummy text of the printin and typesetting industry. Lorem Ipsum has be the industrys standard dummy text ever since the 1560s.',
    isVisible: false,
    id: 6
  },
];
