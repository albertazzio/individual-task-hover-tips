import { ModalTexts } from '../models/modaltexts';

export const EXAMPLE_MODAL_TEXTS: ModalTexts = {
  title: 'Example',
  bodyText: 'This is a modal window!',
  isCancel: false,
  isOkBtn: true,
  data: null,
}
