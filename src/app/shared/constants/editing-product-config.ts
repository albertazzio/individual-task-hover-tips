import { ModalProductConfig } from '../models/modal-product-config';

export const EDITING_PRODUCT_MODAL_DATA: ModalProductConfig = {
  title: 'Editing Product',
  isSaveBtn: true,
  isCancel: true,
  isAddBtn: false,
  data: null,
}
