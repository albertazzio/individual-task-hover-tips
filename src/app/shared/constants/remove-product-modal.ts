import { ModalConfig } from '../models/modal-config';

export const REMOVE_MODAL_DATA: ModalConfig = {
  title: 'Confirm action',
  bodyText: 'Are you sure to remove this product?',
  isOkBtn: true,
  isCancel: true,
  data: null,
}
