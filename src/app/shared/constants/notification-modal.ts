import { ModalTexts } from '../models/modaltexts';

export const MODAL_TEXTS: ModalTexts = {
  title: 'Notification',
  bodyText: 'Are you sure you want to remove this lead message?',
  isCancel: false,
  isOkBtn: true,
  data: null,
}
