import { ModalProductConfig } from '../models/modal-product-config';

export const ADDING_PRODUCT_MODAL_DATA: ModalProductConfig = {
  title: 'Add new product',
  isSaveBtn: false,
  isCancel: true,
  isAddBtn: true,
  data: null,
}
