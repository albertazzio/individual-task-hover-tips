import { Component, HostListener, OnInit } from '@angular/core';

import { TestData } from './shared/models/api';
import { KEYBOARD_ESCAPE } from './shared/constants/keyboard';
import { SpinnerService } from './shared/services/spinner.service/spinner.service';
import { SidebarService } from './shared/services/sidebar.service/sidebar.service';
import { ProductsService } from './shared/services/products.service/products.service';
import { NotificationModalService } from './shared/services/notification-modal.service/notification-modal.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  public B: any = {"data":[{"name":"Face to Face","id":1,"createdById":"00000000-0000-0000-0000-000000000000","createdDateTime":"2022-04-12T23:14:10.3700721+00:00","updatedMemo":"Created by migration","updatedDateTime":"2022-04-12T23:14:10.3700721+00:00"},{"name":"Lab Result","id":2,"createdById":"00000000-0000-0000-0000-000000000000","createdDateTime":"2022-04-12T23:14:10.3700721+00:00","updatedMemo":"Created by migration","updatedDateTime":"2022-04-12T23:14:10.3700721+00:00"},{"name":"Sleep Study","id":6,"createdById":"be00fd17-47ca-4e5e-ad4d-0b1537d567e5","createdDateTime":"2022-04-12T00:00:00+03:00","updatedMemo":"created","updatedDateTime":"2022-04-12T00:00:00+03:00"}],"isOk":true};

  constructor (
    public sidebarService: SidebarService,
    public productsService: ProductsService,
    public notificationModalService: NotificationModalService,
    public spinnerService: SpinnerService,
  ) {}

  public ngOnInit (): void {
    this.sidebarService.setSidebarStatus();
  }

  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key === KEYBOARD_ESCAPE) {
      this.notificationModalService.isShowModal = false;
    }
  }
}
