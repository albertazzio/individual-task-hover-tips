import { Component, OnInit } from '@angular/core';

import { finalize, Subscription } from 'rxjs';
import { Product } from '../../shared/models/api';
import { getUniqId } from '../../shared/helpers/helpers';
import { WOMEN, MEN } from '../../shared/constants/keyboard';
import { ModalResponse } from '../../shared/models/modal-config';
import { REMOVE_MODAL_DATA } from '../../shared/constants/remove-product-modal';
import { SpinnerService } from '../../shared/services/spinner.service/spinner.service';
import { ProductsService } from '../../shared/services/products.service/products.service';
import { ModalProductChanging, ModalProductConfig } from '../../shared/models/modal-product-config';
import { NotificationModalService } from '../../shared/services/notification-modal.service/notification-modal.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
})
export class ProductsPage implements OnInit {
  public arr: Product[] = [];
  public products: Product[] = [];
  public editingProduct: any = [];
  public categories: string[] = [];
  public isFilterByMen: boolean = false;
  public isProductModalShow: boolean = false;
  public errorMessage: string | undefined = undefined;
  public changeProductData: ModalProductConfig = new ModalProductConfig();

  private deleteSubscribeOnRemoveProduct: Subscription;

  constructor(
    public spinnerService: SpinnerService,
    public productsService: ProductsService,
    public notificationModalService: NotificationModalService,
  ) {}

  public ngOnInit(): void {
    this.showAllTableData();
    this.subscribeOnRemoveProduct();
  }

  public ngOnDestroy(): void {
    this.deleteSubscribeOnRemoveProduct.unsubscribe();
  }

  public showAllTableData(): void {
    this.errorMessage = undefined;

    setTimeout(() => {
      this.spinnerService.isSpinnerActive = true;
    }, 0);
    this.productsService
      .getProducts$('')
      .pipe(finalize(() => this.spinnerService.isSpinnerActive = false))
      .subscribe((val ) => {
          this.products = val.entries;

          let tempArray: string[] = [];

          val?.entries.forEach(item => tempArray.push(item.category));
          this.categories = [...new Set(tempArray)];
        },
        (error) => {
          this.errorMessage = error;
        }
      );
  }

  public filterByGender(): void {
    this.isFilterByMen = !this.isFilterByMen;
    this.arr = this.products.filter((it) => {

      if (this.isFilterByMen) {
        return it.gender === MEN;
      } else {
        return it.gender === WOMEN;
      }
    })
  }

  public removeProduct(data: Product): void {
    this.notificationModalService.openModal({
        title: REMOVE_MODAL_DATA.title,
        bodyText: REMOVE_MODAL_DATA.bodyText,
        isOkBtn: REMOVE_MODAL_DATA.isOkBtn,
        isCancel: REMOVE_MODAL_DATA.isCancel,
        data: data,
      }
    );
  }

  public resetAllDataProducts(): void {
    this.products = [];
    this.arr = [];
  }

  public addNewProduct(value: Product): void {
    if (this.editingProduct) {
      let index = this.products.findIndex(el => el.id === this.editingProduct.id);

      if (index !== -1) {
        this.products[index].link = value.link;
        this.products[index].author = value.author;
        this.products[index].category = value.category;
        this.products[index].description = value.description;
        this.products[index].gender = value.gender;
      }
    } else {
      value.id = getUniqId();
      this.products.unshift(value);
    }
    this.closeAddingForm();
  }

  public openProductModalEdit(data: ModalProductChanging): void {
    this.editingProduct = data.data;

    this.openModalProduct({
      title: data.config.title,
      isCancel: data.config.isCancel,
      isSaveBtn: data.config.isSaveBtn,
      isAddBtn: data.config.isAddBtn,
      data: data,
    });
  }

  public openModalProduct(value: ModalProductConfig): void{
    this.changeProductData = value;
    this.isProductModalShow = !this.isProductModalShow;
  }

  public showDataByCategory(category: string): void {
    this.arr = [];

    setTimeout(() => {
      this.spinnerService.isSpinnerActive = true;
    }, 0);

    this.productsService
      .getProducts$(category)
      .pipe(finalize(() => this.spinnerService.isSpinnerActive = false))
      .subscribe((val) => {
          this.products = val.entries;
        }
      );

    this.spinnerService.isSpinnerActive = false;
  }

  public subscribeOnRemoveProduct(): void {
    this.deleteSubscribeOnRemoveProduct = this.notificationModalService.removeProduct.subscribe((res: ModalResponse) => {
      if (!res.isOk) return;

      if (this.notificationModalService.confirmModalData.data === null) return;

      this.removeDataFromTable(res.data);
    })
  }

  public removeDataFromTable(product: Product): void {
    this.spinnerService.isSpinnerActive = true;

    setTimeout(() => {
      this.products = this.products.filter(item => item.id !== product.id);
      this.spinnerService.isSpinnerActive = false;
    }, 500);
  }

  public closeAddingForm(): void {
    this.isProductModalShow = false;
  }
}
