import { UsersPage } from './users/users.page';
import { AccountsPage } from './accounts/accounts.page';
import { PatientsPage } from './patients/patients.page';
import { ProductsPage } from './products/products.page';
import { RequestsPage } from './requests/requests.page';
import { SuppliersPage } from './suppliers/suppliers.page';
import { MyProfilePage } from './my-profile/my-profile.page';
import { NewRequestPage } from './new-request/new-request.page';
import { PriceSheetsPage } from './price-sheets/price-sheets.page';
import { InvoiceSummaryPage } from './invoice-summary/invoice-summary.page';
import { UserManagementPage } from './user-management/user-management.page';
import { PriceSheetsSearchPage } from './price-sheets-search/price-sheets-search.page';

export const PAGES = [
  UsersPage,
  AccountsPage,
  PatientsPage,
  ProductsPage,
  RequestsPage,
  MyProfilePage,
  SuppliersPage,
  NewRequestPage,
  PriceSheetsPage,
  InvoiceSummaryPage,
  UserManagementPage,
  PriceSheetsSearchPage,
]
