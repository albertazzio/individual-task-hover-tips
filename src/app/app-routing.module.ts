import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersPage } from './pages/users/users.page';
import { PatientsPage } from './pages/patients/patients.page';
import { AccountsPage } from './pages/accounts/accounts.page';
import { ProductsPage } from './pages/products/products.page';
import { RequestsPage } from './pages/requests/requests.page';
import { SuppliersPage } from './pages/suppliers/suppliers.page';
import { MyProfilePage } from './pages/my-profile/my-profile.page';
import { NewRequestPage } from './pages/new-request/new-request.page';
import { PriceSheetsPage } from './pages/price-sheets/price-sheets.page';
import { InvoiceSummaryPage } from './pages/invoice-summary/invoice-summary.page';
import { UserManagementPage } from './pages/user-management/user-management.page';
import { PriceSheetsSearchPage } from './pages/price-sheets-search/price-sheets-search.page';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/products',
    pathMatch: 'full',
  },
  {
    path:'users',
    component: UsersPage,
  },
  {
    path:'patients',
    component: PatientsPage,
  },
  {
    path:'accounts',
    component: AccountsPage,
  },
  {
    path:'products',
    component: ProductsPage,
  },
  {
    path:'requests',
    component: RequestsPage,
  },
  {
    path:'suppliers',
    component: SuppliersPage,
  },
  {
    path:'my-profile',
    component: MyProfilePage,
  },
  {
    path:'new-request',
    component: NewRequestPage,
  },
  {
    path:'price-sheets',
    component: PriceSheetsPage,
  },
  {
    path:'invoice-summary',
    component: InvoiceSummaryPage,
  },
  {
    path:'user-management',
    component: UserManagementPage,
  },
  {
    path:'price-sheet-search',
    component: PriceSheetsSearchPage,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
