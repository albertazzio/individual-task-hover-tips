import { Component } from '@angular/core';

import { SidebarTitle } from 'src/app/shared/models/sidebar-title';
import { SIDEBAR_TITLES } from 'src/app/shared/constants/sidebar-titles';
import { SidebarService } from '../../shared/services/sidebar.service/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  public readonly ARR_SIDEBAR_TITLES: SidebarTitle[] = SIDEBAR_TITLES;

  constructor (
    public sidebarService: SidebarService,
    ) {}
}
