import { Component, EventEmitter, Output } from '@angular/core';

import { ModalConfig } from 'src/app/shared/models/modal-config';
import { EXAMPLE_MODAL_TEXTS } from '../../shared/constants/example-modal';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Output() public openConfirmModal: EventEmitter<ModalConfig> = new EventEmitter<ModalConfig>();
  @Output() public changeSidebarStatus: EventEmitter<void> = new EventEmitter<void>();

  public readonly EXAMPLE_MODAL_TEXTS: ModalConfig = EXAMPLE_MODAL_TEXTS;
}
