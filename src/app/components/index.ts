import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';


export const APP_COMPONENTS = [
  HeaderComponent,
  SidebarComponent,
]
